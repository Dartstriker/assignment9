﻿using Assignment9.Interfaces;
using Assignment9.Realisations;

namespace Assignment9
{
    internal class Program
	{
		static void Main(string[] args)
		{
			IPlayable game = new Game(new ConsoleIOWithLogging(new Logger()), new Config());
			game.Play();
		}
	}
}