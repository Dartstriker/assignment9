﻿using Assignment9.Interfaces;

namespace Assignment9.Realisations;

public class ConsoleIO : IGameIO
{
    public virtual string Input()
    {
        return Console.ReadLine();
    }

    public virtual void Output(string output)
    {
        Console.WriteLine(output);
    }
}