﻿using Assignment9.Interfaces;
using Microsoft.Extensions.Configuration;

namespace Assignment9.Realisations;

public class Config : IConfig
{
	public int GetInt(string key)
	{
		var configuration = new ConfigurationBuilder()
			.AddJsonFile($"appsettings.json");

		var config = configuration.Build();
		return Int32.Parse(config.GetSection(key).Value);
	}
}