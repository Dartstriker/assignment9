﻿using Assignment9.Interfaces;

namespace Assignment9.Realisations;

public class ConsoleIOWithLogging : ConsoleIO
{
	private ILoggable _logger;

	public ConsoleIOWithLogging(ILoggable logger)
	{
		_logger = logger;
	}

    public override string Input()
    {
	    var input = base.Input();
		_logger.Log(input);
		return input;
    }

    public override void Output(string output)
    {
		_logger.Log(output);
		base.Output(output);
    }
}