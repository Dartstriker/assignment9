﻿using Assignment9.Interfaces;

namespace Assignment9.Realisations;

public class Logger : ILoggable
{
	public void Log(string message)
	{
		Console.WriteLine($"Logged: {message}");
	}
}