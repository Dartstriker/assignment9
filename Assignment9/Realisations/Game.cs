﻿using Assignment9.Interfaces;

namespace Assignment9.Realisations;

public class Game : IPlayable
{
    private IGameIO _gameIO;
    private IConfig _config;

    public Game(IGameIO gameIO, IConfig config)
    {
        _gameIO = gameIO;
        _config = config;
    }

    public void Play()
    {
        bool won = false;
        var num = 0;
        var tries = _config.GetInt("Tries");
        var gameNum = new Random().Next(_config.GetInt("MinNumber"), _config.GetInt("MaxNumber"));
        while (!won && tries > 0)
        {
            _gameIO.Output("Please input your number");
            int.TryParse(_gameIO.Input(), out num);
            if (num == gameNum)
            {
                _gameIO.Output("You won! Press enter to exit");
                won = true;
                _gameIO.Input();
                return;
            }
            else if (num < gameNum)
            {
                _gameIO.Output("Your number is lower");
            }
            else
            {
                _gameIO.Output("Your number is higher");
            }
            tries--;
        }
        _gameIO.Output("You lost! Press enter to exit");
        _gameIO.Input();
		return;
    }
}