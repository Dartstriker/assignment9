﻿namespace Assignment9.Interfaces;

public interface IConfig
{
	public int GetInt(string key);
}