﻿namespace Assignment9.Interfaces;

public interface ILoggable
{
	public void Log(string message);
}