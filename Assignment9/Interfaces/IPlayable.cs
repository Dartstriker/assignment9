﻿namespace Assignment9.Interfaces;

public interface IPlayable
{
    public void Play();
}