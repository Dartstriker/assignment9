﻿namespace Assignment9.Interfaces;

public interface IGameIO
{
    public string Input();

    public void Output(string output);
}